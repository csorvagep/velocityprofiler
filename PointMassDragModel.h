#ifndef POINTMASSDRAGMODEL_H
#define POINTMASSDRAGMODEL_H

#include "conf.h"
#include "Model.h"

class PointMassDragModel : public Model
{
    double tangAccelMax = TANG_ACCEL_MAX;
    double tangAccelMin = TANG_ACCEL_MIN;
    double normalAccelMax = NORMAL_ACCEL_MAX;
    double dragCoef = DRAG_COEF;
public:
    double getCriticalVelocity(double curvature) const override;
    void stepForward(double u, double curvature) override;
    void stepBackward(double u, double curvature) override;
private:
    double getMaxTangentialAccel(double u) const;
    std::pair<double, double> calculateDelta(double u, double curvature) const;
};

#endif // POINTMASSDRAGMODEL_H
