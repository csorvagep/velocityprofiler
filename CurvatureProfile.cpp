#include "CurvatureProfile.h"

#include <string>
#include <sstream>
#include <cmath>

CurvatureProfile::CurvatureProfile()
{}

CurvatureProfile::CurvatureProfile(std::istream &stream)
    : AbstractCurvatureProfile(stream)
{}

double CurvatureProfile::getCurvature(double position) const
{
    return getValue(position);
}


