#pragma once

#define EPS             0.0001
#define TIME_STEP       0.05
#define DIST_STEP       0.05
#define RADIUS_SAMPLE   0.1

#define TANG_ACCEL_MAX      16.0
#define TANG_ACCEL_MIN      -18.0
#define NORMAL_ACCEL_MAX    30.0
#define DRAG_COEF           0.0021
