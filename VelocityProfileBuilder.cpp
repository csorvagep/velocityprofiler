#include "VelocityProfileBuilder.h"
#include "PointMassModel.h"

#include <cmath>

VelocityProfileBuilder::VelocityProfileBuilder(const AbstractCurvatureProfile &curvatureProfile)
    :curvature(curvatureProfile), z2_0(0), z2_f(0)
{
    model = std::make_unique<PointMassModel>();
}

VelocityProfile VelocityProfileBuilder::build()
{
    VelocityProfile profile;

    collectLocalMinima();
    for(size_t i = 0; i < localMinima.size(); i++) {
        buildSubProfile(i);
        profile.appendProfileByMin(subProfiles.back());
    }

    return profile;
}

void VelocityProfileBuilder::setStartVelocity(double z2_0)
{
    this->z2_0 = z2_0;
}

void VelocityProfileBuilder::setEndVelocity(double z2_f)
{
    this->z2_f = z2_f;
}

void VelocityProfileBuilder::setModel(std::unique_ptr<Model> model)
{
    this->model = std::move(model);
}

std::vector<VelocityProfile> VelocityProfileBuilder::getSubProfiles() const
{
    return subProfiles;
}

std::vector<Profile::Point> VelocityProfileBuilder::getCrossSections() const
{
    return crossSections;
}

void VelocityProfileBuilder::collectLocalMinima()
{
    localMinima.clear();
    localMinima.emplace_back(curvature.getFirstPosition(), z2_0);

    for(auto &curvaturePoint : curvature.getLocalMaxima()) {
        double criticalVelocity = model->getCriticalVelocity(curvaturePoint.second);
        localMinima.emplace_back(curvaturePoint.first, criticalVelocity);
    }

    localMinima.emplace_back(curvature.getLastPosition(), z2_f);
}

void VelocityProfileBuilder::buildSubProfile(size_t index)
{
    subProfiles.emplace_back();
    buildSubProfileForward(index);
    buildSubProfileBackward(index);
}

void VelocityProfileBuilder::buildSubProfileForward(size_t index)
{
    VelocityProfile& subProfile = subProfiles.back();
    model->setState(localMinima[index]);
    for(size_t i = index; i < localMinima.size()-1; i++) {
        while(model->getState().first < localMinima[i+1].first ) {
            subProfile.addProfilePointBack(model->getState());
            model->stepForwardOptimal(curvature.getCurvature(model->getState().first));
        }

        if(model->getState().second >= localMinima[i+1].second)
            break;
    }
}

void VelocityProfileBuilder::buildSubProfileBackward(size_t index)
{
    VelocityProfile& subProfile = subProfiles.back();
    model->setState(localMinima[index]);
    for(size_t i = index; i > 0; i--) {
        while(model->getState().first > localMinima[i-1].first ) {
            subProfile.addProfilePointFront(model->getState());
            model->stepBackwardOptimal(curvature.getCurvature(model->getState().first));
        }

        if(model->getState().second >= localMinima[i-1].second)
            break;
    }
}

double VelocityProfileBuilder::getCriticalSpeed(double position)
{
    return std::sqrt(std::fabs(1/curvature.getCurvature(position)));
}
