#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qcustomplot.h"
#include "AbstractCurvatureProfile.h"
#include "Path.h"
#include "VelocityProfile.h"
#include <QString>
#include <memory>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void UpdatePlots(void);
    void LoadFile(void);

private:
    void UpdateVelocityProfile();
    void UpdateCurvatureProfile();
    void UpdatePath();

    Ui::MainWindow *ui;
    QString filePath;
    bool filePathChanged;

    std::unique_ptr<AbstractCurvatureProfile> curvature;
    Path path;
    VelocityProfile velocity;
};

#endif // MAINWINDOW_H
