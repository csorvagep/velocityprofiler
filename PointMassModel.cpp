#include "PointMassModel.h"

#include <cmath>
#include <exception>

double PointMassModel::getCriticalVelocity(double curvature) const
{
    return std::sqrt(1 / std::fabs(curvature));
}

void PointMassModel::stepForward(double u, double curvature)
{
    std::pair<double, double> delta = calculateDelta(u,curvature);

    currentState.first += delta.first;
    currentState.second += delta.second;
}

void PointMassModel::stepBackward(double u, double curvature)
{
    std::pair<double, double> delta = calculateDelta(u,curvature);

    currentState.first -= delta.first;
    currentState.second -= delta.second;
}

std::pair<double, double> PointMassModel::calculateDelta(double u, double curvature)
{
    std::pair<double, double> delta;
    delta.first = currentState.second * timeStep;

    double det = currentState.second * currentState.second * curvature;
    if (std::fabs(det) >= 1.0)
    {
        delta.second = 0;
    }
    else
    {
        delta.second = u * std::sqrt(1 - det * det) * timeStep;
    }
    return delta;
}
