#include "RadiusProfile.h"

RadiusProfile::RadiusProfile()
{}

RadiusProfile::RadiusProfile(std::istream &stream)
    : AbstractCurvatureProfile(stream)
{}

double RadiusProfile::getRadius(double position) const
{
    return getValue(position);
}

double RadiusProfile::getCurvature(double position) const
{
    return 1.0 / getValue(position);
}
