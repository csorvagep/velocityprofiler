#ifndef RADIUSPROFILE_H
#define RADIUSPROFILE_H

#include "AbstractCurvatureProfile.h"

class RadiusProfile : public AbstractCurvatureProfile
{
public:
    RadiusProfile();
    RadiusProfile(std::istream& stream);

    double getRadius(double position) const;
    double getCurvature(double position) const override;
};

#endif // RADIUSPROFILE_H
