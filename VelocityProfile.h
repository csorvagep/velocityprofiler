#ifndef VELOCITYPROFILE_H
#define VELOCITYPROFILE_H

#include <utility>
#include <deque>
#include <vector>
#include "Profile.h"

class VelocityProfile : public Profile
{
public:
    VelocityProfile();
    VelocityProfile(std::istream& stream);
};

#endif // VELOCITYPROFILE_H
