#include "Profile.h"

#include <string>
#include <sstream>
#include <cmath>
#include "conf.h"

Profile::Profile()
{}

Profile::Profile(std::istream &stream)
{
    while(stream.good()) {
        std::string line;
        std::getline(stream, line);
        if(!line.empty()) {
            std::stringstream ss(line);
            addPointFromStream(ss);
        }
    }
}

double Profile::getValue(double position) const
{
    for(size_t i = 0; i < profile.size()-1; i++) {
        if(position < profile[i+1].first) {
            if(i != 0 && isDoublePointProblem(i, position)) {
                if(std::fabs(profile[i].second) > std::fabs(profile[i-1].second))
                    return profile[i].second;
                else
                    return profile[i-1].second;
            } else {
                return interpolateValue(i, position);
            }
        }
    }
    return interpolateValue(profile.size()-1, position);
}

double Profile::getFirstPosition() const
{
    return profile.front().first;
}

double Profile::getLastPosition() const
{
    return profile.back().first;
}

std::vector<double> Profile::getPositionVector() const
{
    std::vector<double> pos;
    for(const auto& point : profile) {
        pos.push_back(point.first);
    }
    return pos;
}

std::vector<double> Profile::getValueVector() const
{
    std::vector<double> val;
    for(const auto& point : profile) {
        val.push_back(point.second);
    }
    return val;
}

std::vector<double> Profile::getPositionVectorSampled(double sample) const
{
    std::vector<double> pos;

    if(profile.empty())
        return pos;

    for(double s = profile.front().first; s < profile.back().first; s += sample) {
        pos.push_back(s);
    }
    return pos;
}

std::vector<double> Profile::getValueVectorSampled(double sample) const
{
    std::vector<double> positionVector = getPositionVectorSampled(sample);
    std::vector<double> val;

    if(positionVector.empty())
        return val;

    for(double pos : positionVector) {
        val.push_back(getValue(pos));
    }
    return val;
}

void Profile::addProfilePointFront(const Point &point)
{
    profile.push_front(point);
}

void Profile::addProfilePointBack(const Point &point)
{
    profile.push_back(point);
}

//void Profile::replaceSection(const Profile &other)
//{
//    if(other.profile.empty())
//        return;

//    size_t beginIndex = getStartIndex(other.getFirstPosition());
//    size_t endIndex = getEndIndex(other.getLastPosition());
//    profile.erase(profile.begin() + beginIndex, profile.begin() + endIndex);
//    profile.insert(profile.begin() + beginIndex, other.profile.begin(), other.profile.end());
//}

void Profile::replaceEndFrom(const Profile &other, double from)
{
    size_t thisBeginIndex = this->getStartIndex(from);
    size_t otherBeginIndex = other.getStartIndex(from);
    profile.erase(profile.begin() + thisBeginIndex, profile.end());
    profile.insert(profile.end(), other.profile.begin() + otherBeginIndex, other.profile.end());
}

//void Profile::replaceBegin(const Profile &other)
//{
//    size_t endIndex = getEndIndex(other.getLastPosition());
//    profile.erase(profile.begin(), profile.begin() + endIndex);
//    profile.insert(profile.begin(), other.profile.begin(), other.profile.end());
//}

ProfileCrossSection Profile::getCrossSection(const Profile &other) const
{
    size_t thisIndex = this->getStartIndex(other.getFirstPosition());
    size_t otherIndex = other.getStartIndex(this->getFirstPosition());

    if(thisIndex == this->profile.size())
        return ProfileCrossSection(false);

    bool isThisHigher = this->getValueAt(thisIndex) > other.getValueAt(otherIndex);
    while(isThisHigher ^ (this->getValueAt(thisIndex) <= other.getValueAt(otherIndex))) {
        while(this->getPositionAt(thisIndex) <= other.getPositionAt(otherIndex)) {
            if(++thisIndex == this->profile.size())
                return ProfileCrossSection(!isThisHigher);
        }
        while(other.getPositionAt(otherIndex) <= this->getPositionAt(thisIndex)) {
            if(++otherIndex == other.profile.size())
                return ProfileCrossSection(!isThisHigher);
        }
    }

    //TODO lehetne okosítani egy metszéspont számítással
    double position = std::min(this->getPositionAt(thisIndex), other.getPositionAt(otherIndex));
    double value = getValue(position);
    return ProfileCrossSection(position, value, !isThisHigher);
}

void Profile::appendProfileByMin(const Profile &other)
{
    if(this->profile.empty()) {
        this->profile = other.profile;
        return;
    }

    ProfileCrossSection crossSection = getCrossSection(other);
    if(crossSection.isCrossSection()) {
        replaceEndFrom(other, crossSection.getPosition());
    } else {
        if(!crossSection.isThisLowerFirst()) {
            replaceEndFrom(other, other.getFirstPosition());
        } else {
            replaceEndFrom(other, this->getLastPosition());
        }
    }
}

//Profile Profile::getPartProfile(double posA, double posB) const
//{
//    Profile partProfile;
//    auto beginIterator = profile.begin() + getStartIndex(posA);
//    auto endIterator = profile.begin() + getEndIndex(posB);
//    partProfile.profile = ProfileContainer(beginIterator, endIterator);
//    return partProfile;
//}

void Profile::addPointFromStream(std::istream &stream)
{
    char separator;
    double pos, val;
    stream >> val >> separator;
    stream >> pos;
    profile.emplace_back(pos,val);
}

double Profile::interpolateValue(size_t index, double position) const
{
    double x1 = profile[index].first;
    double x2 = profile[index+1].first;
    double f_x1 = profile[index].second;
    double f_x2 = profile[index+1].second;
    if(x1 == x2)
        return (f_x1 + f_x2)*0.5;

    return (f_x2 - f_x1)/(x2-x1)*(position-x1)+f_x1;
}

bool Profile::isDoublePointProblem(size_t index, double position) const
{
    bool isEqual = profile[index].first == profile[index-1].first;
    bool isNear = std::fabs(position - profile[index].first) < EPS;
    return isEqual && isNear;
}

size_t Profile::getStartIndex(double startPos) const
{
    size_t index;
    for(index = 0; index < profile.size(); index++) {
        if(profile[index].first >= startPos)
            break;
    }
    return index;
}

size_t Profile::getEndIndex(double endPos) const
{
    size_t index;
    for(index = profile.size(); index > 0; index--) {
        if(profile[index-1].first <= endPos)
            break;
    }
    return index;
}

double Profile::getPositionAt(size_t index) const
{
    return profile[index].first;
}

double Profile::getValueAt(size_t index) const
{
    return profile[index].second;
}
