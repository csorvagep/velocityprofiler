#ifndef ABSTRACTCURVATUREPROFILE_H
#define ABSTRACTCURVATUREPROFILE_H

#include "Profile.h"
#include <iostream>

class AbstractCurvatureProfile : public Profile
{
public:
    AbstractCurvatureProfile();
    AbstractCurvatureProfile(std::istream& stream);

    virtual double getCurvature(double position) const = 0;

    std::vector<Profile::Point> getLocalMaxima() const;
private:
    bool isLocalMaximum(double position) const;
};

#endif // ABSTRACTCURVATUREPROFILE_H
