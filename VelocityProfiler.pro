#-------------------------------------------------
#
# Project created by QtCreator 2015-12-05T16:06:39
#
#-------------------------------------------------

QT       += core gui printsupport
CONFIG   += c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VelocityProfiler
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    CurvatureProfile.cpp \
    Path.cpp \
    qcustomplot/qcustomplot.cpp \
    VelocityProfile.cpp \
    VelocityProfileBuilder.cpp \
    PointMassModel.cpp \
    RadiusProfile.cpp \
    Profile.cpp \
    profileplotter.cpp \
    AbstractCurvatureProfile.cpp \
    ProfileCrossSection.cpp \
    Model.cpp \
    PointMassDragModel.cpp

HEADERS  += mainwindow.h \
    CurvatureProfile.h \
    Path.h \
    conf.h \
    qcustomplot/qcustomplot.h \
    VelocityProfile.h \
    VelocityProfileBuilder.h \
    PointMassModel.h \
    RadiusProfile.h \
    Profile.h \
    profileplotter.h \
    AbstractCurvatureProfile.h \
    ProfileCrossSection.h \
    Model.h \
    PointMassDragModel.h

FORMS    += mainwindow.ui

INCLUDEPATH += qcustomplot
