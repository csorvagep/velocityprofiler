#include "PointMassDragModel.h"

#include <cmath>

double PointMassDragModel::getCriticalVelocity(double curvature) const
{
    return std::sqrt(normalAccelMax / std::fabs(curvature));
}

void PointMassDragModel::stepForward(double u, double curvature)
{
    auto delta = calculateDelta(u, curvature);
    currentState.first += delta.first;
    currentState.second += delta.second;
}

void PointMassDragModel::stepBackward(double u, double curvature)
{
    auto delta = calculateDelta(u, curvature);
    currentState.first -= delta.first;
    currentState.second -= delta.second;
}

double PointMassDragModel::getMaxTangentialAccel(double u) const
{
    double velocitySquare =  currentState.second * currentState.second;
    if(u > 0) {
        return tangAccelMax - dragCoef * velocitySquare;
    } else {
        return tangAccelMin - dragCoef * velocitySquare;
    }
}

std::pair<double, double> PointMassDragModel::calculateDelta(double u, double curvature) const
{
    std::pair<double, double> delta;
    delta.first = currentState.second * timeStep;

    double atMaxSq = getMaxTangentialAccel(u);
    atMaxSq *= atMaxSq;

    double anMaxSq = normalAccelMax * normalAccelMax;

    double anSq = currentState.second * currentState.second * curvature;
    anSq *= anSq;

    if(anMaxSq > anSq) {
        delta.second = u * std::sqrt(atMaxSq - (atMaxSq / anMaxSq) * anSq) * timeStep;
    } else {
        delta.second = 0;
    }
    return delta;
}
