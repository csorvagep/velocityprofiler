#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <fstream>
#include <QDebug>
#include <QFileDialog>
#include "VelocityProfileBuilder.h"
#include "profileplotter.h"
#include "RadiusProfile.h"
#include "CurvatureProfile.h"
#include "PointMassDragModel.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Setup velocity profile graph
    QCustomPlot* profilePlot = ui->customPlot_profile;
    profilePlot->plotLayout()->insertRow(0);
    profilePlot->plotLayout()->addElement(0, 0, new QCPPlotTitle(profilePlot, "Velocity Profile"));
    profilePlot->xAxis->setLabel("z1 [m]");
    profilePlot->yAxis->setLabel("z2 [m/s]");
    profilePlot->xAxis2->setVisible(true);
    profilePlot->xAxis2->setTickLabels(false);
    profilePlot->yAxis2->setVisible(true);
    profilePlot->yAxis2->setTickLabels(false);
    profilePlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);

    // Setup curvature profile graph
    QCustomPlot* curvPlot = ui->customPlot_curv;
    curvPlot->plotLayout()->insertRow(0);
    curvPlot->plotLayout()->addElement(0, 0, new QCPPlotTitle(curvPlot, "Curvature"));
    curvPlot->xAxis->setLabel("s [m]");
    curvPlot->yAxis->setLabel("c [1/m]");
    curvPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);

    // Setup path graph
    QCustomPlot* pathPlot = ui->customPlot_path;
    pathPlot->plotLayout()->insertRow(0);
    pathPlot->plotLayout()->addElement(0, 0, new QCPPlotTitle(pathPlot, "Path"));
    pathPlot->xAxis->setLabel("x [m]");
    pathPlot->yAxis->setLabel("y [m]");
    pathPlot->addPlottable(new QCPCurve(pathPlot->xAxis, pathPlot->yAxis));
    pathPlot->plottable(0)->setPen(QPen(Qt::blue));

    QObject::connect(ui->pushButton_Update, SIGNAL(pressed()), this, SLOT(UpdatePlots()) );
    QObject::connect(ui->pushButton_Load, SIGNAL(pressed()), this, SLOT(LoadFile()) );

    filePath = "../robonaut.txt";
    filePathChanged = true;

    UpdatePlots();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::UpdateVelocityProfile()
{
    VelocityProfileBuilder builder(*curvature);
    if(ui->checkBox_DragModel->isChecked()) {
        builder.setModel(std::make_unique<PointMassDragModel>());
    }
    builder.setStartVelocity(ui->doubleSpinBox_z20->value());
    builder.setEndVelocity(ui->doubleSpinBox_z2f->value());
    VelocityProfile profile = builder.build();

    ProfilePlotter plotter(this, ui->customPlot_profile);
    plotter.clearAll();
//    QPen pen(Qt::black);
//    pen.setStyle(Qt::DashLine);
//    plotter.setPen(pen);
//    for(auto &subProfile : builder.getSubProfiles()) {
//        plotter.addPlot(subProfile);
//    }

    plotter.setColor(Qt::red);
    plotter.addPoints(builder.getCrossSections());

    plotter.setColor(Qt::blue);
    plotter.addPlot(profile);

    plotter.replot();
}

void MainWindow::UpdateCurvatureProfile()
{
    ProfilePlotter plotter(this, ui->customPlot_curv);
    plotter.clearAll();
    plotter.addPlot(*curvature);

    plotter.setColor(Qt::red);
    plotter.addPoints(curvature->getLocalMaxima());

    plotter.replot();
}

void MainWindow::UpdatePath()
{
    QCustomPlot *plot = ui->customPlot_path;
    plot->plottable(0)->clearData();


    QVector<double> x = QVector<double>::fromStdVector(path.getXVector());
    QVector<double> y = QVector<double>::fromStdVector(path.getYVector());

    static_cast<QCPCurve*>(plot->plottable(0))->setData(x,y);
    plot->plottable(0)->rescaleAxes();
    plot->replot();
}

void MainWindow::UpdatePlots()
{
    if(filePathChanged) {
        std::ifstream fileStream(filePath.toStdString());
        if(ui->checkBox_fromRadius->isChecked()) {
            curvature = std::make_unique<RadiusProfile>(fileStream);
        } else {
            curvature = std::make_unique<CurvatureProfile>(fileStream);
        }
        UpdateCurvatureProfile();

        path = Path(*curvature);
        UpdatePath();
        filePathChanged = false;
    }

    UpdateVelocityProfile();
}

void MainWindow::LoadFile()
{
    QString tempPath = QFileDialog::getOpenFileName(this, tr("Open Curvature Profile"), "../", tr("Text Files (*.txt)"));
    if(!tempPath.isEmpty()) {
        filePath = tempPath;
        filePathChanged = true;
    }
}
