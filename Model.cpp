#include "Model.h"

Model::Model()
    : currentState(0,0)
{}

Profile::Point Model::getState() const
{
    return currentState;
}

void Model::setState(const Profile::Point &state)
{
    currentState = state;
}

void Model::stepForwardOptimal(double curvature)
{
    stepForward(1, curvature);
}

void Model::stepBackwardOptimal(double curvature)
{
    stepBackward(-1, curvature);
}
