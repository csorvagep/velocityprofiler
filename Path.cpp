#include "Path.h"

#include <cmath>

Path::Path()
{}

Path::Path(const AbstractCurvatureProfile &curvature, double distStep)
{
    calculateFromCurvature(curvature, distStep);
}

void Path::calculateFromCurvature(const AbstractCurvatureProfile &curvature, double distStep)
{
    double degree = 0;
    PathPoint p = {0,0};

    points.push_back(p);
    for(double sCurr = curvature.getFirstPosition(); sCurr < curvature.getLastPosition(); sCurr += distStep) {
        degree += curvature.getCurvature(sCurr) * distStep;
        p.first += std::cos(degree) * distStep;
        p.second += std::sin(degree) * distStep;
        points.push_back(p);
    }
}

std::vector<double> Path::getXVector() const
{
    std::vector<double> x;
    for(auto &&p : points)
        x.push_back(p.first);
    return x;
}

std::vector<double> Path::getYVector() const
{
    std::vector<double> y;
    for(auto &&p : points)
        y.push_back(p.second);
    return y;
}

