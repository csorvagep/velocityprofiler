#ifndef CURVATUREPROFILE_H
#define CURVATUREPROFILE_H

#include "AbstractCurvatureProfile.h"

class CurvatureProfile : public AbstractCurvatureProfile
{
public:
    CurvatureProfile();
    CurvatureProfile(std::istream& stream);

    double getCurvature(double position) const override;
};

#endif // CURVATUREPROFILE_H
