#ifndef VELOCITYPROFILEBUILDER_H
#define VELOCITYPROFILEBUILDER_H

#include "AbstractCurvatureProfile.h"
#include "Model.h"
#include "VelocityProfile.h"

#include <memory>

class VelocityProfileBuilder
{
    const AbstractCurvatureProfile& curvature;
    std::unique_ptr<Model> model;
    double z2_0;
    double z2_f;

    std::vector<Profile::Point> localMinima;
    std::vector<VelocityProfile> subProfiles;
    std::vector<Profile::Point> crossSections;
public:
    VelocityProfileBuilder(const AbstractCurvatureProfile& curvatureProfile);

    VelocityProfile build();
    void setStartVelocity(double z2_0);
    void setEndVelocity(double z2_f);
    void setModel(std::unique_ptr<Model> model);
    std::vector<VelocityProfile> getSubProfiles() const;
    std::vector<Profile::Point> getCrossSections() const;

private:
    void collectLocalMinima();
    void buildSubProfile(size_t index);
    void buildSubProfileForward(size_t index);
    void buildSubProfileBackward(size_t index);
    double getCriticalSpeed(double position);
};

#endif // VELOCITYPROFILEBUILDER_H
