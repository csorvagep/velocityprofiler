#ifndef PROFILECROSSSECTION_H
#define PROFILECROSSSECTION_H

class ProfileCrossSection
{
    double position;
    double value;
    bool thisLowerFirst;
    bool crossSection;
public:
    ProfileCrossSection(bool thisLowerFirst);
    ProfileCrossSection(double position, double value, bool thisLowerFirst);

    double getPosition() const;
    double getValue() const;
    bool isThisLowerFirst() const;
    bool isCrossSection() const;
};

#endif // PROFILECROSSSECTION_H
