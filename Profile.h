#ifndef PROFILE_H
#define PROFILE_H

#include <utility>
#include <deque>
#include <vector>
#include <iostream>
#include "conf.h"
#include "ProfileCrossSection.h"

class Profile
{
public:
    using Point = std::pair<double, double>;

    Profile();
    Profile(std::istream& stream);

    double getValue(double position) const;
    double getFirstPosition() const;
    double getLastPosition() const;
    std::vector<double> getPositionVector() const;
    std::vector<double> getValueVector() const;
    std::vector<double> getPositionVectorSampled(double sample = DIST_STEP) const;
    std::vector<double> getValueVectorSampled(double sample = DIST_STEP) const;

    void addProfilePointFront(const Profile::Point& point);
    void addProfilePointBack(const Profile::Point& point);

    //void replaceSection(const Profile& other);
    void replaceEndFrom(const Profile& other, double from);
    //void replaceBegin(const Profile& other);
    ProfileCrossSection getCrossSection(const Profile& other) const;

    void appendProfileByMin(const Profile& other);
    //Profile getPartProfile(double posA, double posB) const;

private:
    using ProfileContainer = std::deque<Profile::Point>;
    ProfileContainer profile;

    void addPointFromStream(std::istream& stream);
    double interpolateValue(size_t index, double position) const;
    bool isDoublePointProblem(size_t index, double position) const;

    size_t getStartIndex(double startPos) const;
    size_t getEndIndex(double endPos) const;

    double getPositionAt(size_t index) const;
    double getValueAt(size_t index) const;
};

#endif // PROFILE_H
