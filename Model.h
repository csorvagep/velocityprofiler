#ifndef MODEL_H
#define MODEL_H

#include "Profile.h"
#include "conf.h"

class Model
{
protected:
    Profile::Point currentState;
    double timeStep = TIME_STEP;

public:
    Model();

    Profile::Point getState() const;
    void setState(const Profile::Point& state);

    virtual double getCriticalVelocity(double curvature) const = 0;
    virtual void stepForward(double u, double curvature) = 0;
    virtual void stepBackward(double u, double curvature) = 0;

    void stepForwardOptimal(double curvature);
    void stepBackwardOptimal(double curvature);
};

#endif // MODEL_H
