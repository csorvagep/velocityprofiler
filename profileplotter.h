#ifndef PROFILEPLOTTER_H
#define PROFILEPLOTTER_H

#include <QObject>
#include <QPen>
#include <qcustomplot.h>
#include "Profile.h"

class ProfilePlotter : public QObject
{
    Q_OBJECT

    QCustomPlot *customPlot;
    QPen pen;
    bool isContinous;
    bool isReScaled;
public:
    ProfilePlotter(QObject *parent, QCustomPlot* plot);

    void setPen(const QPen& pen);
    void setColor(const QColor& color);
    void setContinous(bool continous);

    void addPlot(const Profile& profile);
    void addPoints(std::vector<Profile::Point> points);
    void clearAll();
    void replot();
};

#endif // PROFILEPLOTTER_H
