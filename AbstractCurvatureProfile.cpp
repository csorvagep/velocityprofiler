#include "AbstractCurvatureProfile.h"
#include "conf.h"
#include <cmath>

AbstractCurvatureProfile::AbstractCurvatureProfile()
{}

AbstractCurvatureProfile::AbstractCurvatureProfile(std::istream &stream)
    : Profile(stream)
{}

std::vector<Profile::Point> AbstractCurvatureProfile::getLocalMaxima() const
{
    std::vector<Profile::Point> maxima;

    for(double pos : getPositionVector()) {
        if(isLocalMaximum(pos)) {
            if(maxima.empty() || maxima.back().first != pos)
                maxima.emplace_back(pos,getCurvature(pos));
        }
    }

    return maxima;
}

bool AbstractCurvatureProfile::isLocalMaximum(double position) const
{
    double currentCurv = std::fabs(getCurvature(position));
    double nextCurv = std::fabs(getCurvature(position + DIST_STEP));
    double prevCurv = std::fabs(getCurvature(position - DIST_STEP));

    bool isEqualEachSide = currentCurv == nextCurv && currentCurv == prevCurv;
    bool isGraterEachSide = currentCurv >= nextCurv && currentCurv >= prevCurv;
    return !isEqualEachSide && isGraterEachSide;
}
