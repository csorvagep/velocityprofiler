#pragma once

#include "Model.h"
#include "conf.h"

class PointMassModel : public Model
{
public:
    double getCriticalVelocity(double curvature) const override;
    void stepForward(double u, double curvature) override;
    void stepBackward(double u, double curvature) override;

private:
    std::pair<double, double> calculateDelta(double u, double curvature);
};

