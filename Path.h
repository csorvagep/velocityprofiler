#ifndef PATH_H
#define PATH_H

#include "AbstractCurvatureProfile.h"
#include "conf.h"
#include <vector>
#include <utility>

using PathPoint = std::pair<double, double>;

class Path
{
    std::vector<PathPoint> points;
public:
    Path();
    Path(const AbstractCurvatureProfile& curvature, double distStep = DIST_STEP);

    void calculateFromCurvature(const AbstractCurvatureProfile& curvature, double distStep = DIST_STEP);
    std::vector<double> getXVector() const;
    std::vector<double> getYVector() const;
};

#endif // PATH_H
