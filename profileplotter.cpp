#include "profileplotter.h"

#include <QVector>

ProfilePlotter::ProfilePlotter(QObject *parent, QCustomPlot *plot)
    : QObject(parent), customPlot(plot)
{
    pen = QPen(Qt::blue);
    isContinous = true;
    isReScaled = false;
}

void ProfilePlotter::setPen(const QPen &pen)
{
    this->pen = pen;
}

void ProfilePlotter::setColor(const QColor &color)
{
    this->pen = QPen(color);
}

void ProfilePlotter::setContinous(bool continous)
{
    isContinous = continous;
}

void ProfilePlotter::addPlot(const Profile &profile)
{
    QCPGraph* graph = customPlot->addGraph();
    graph->setPen(pen);
    if(!isContinous) {
        graph->setLineStyle(QCPGraph::lsNone);
        graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCross));
    }

    QVector<double> x = QVector<double>::fromStdVector(profile.getPositionVectorSampled());
    QVector<double> y = QVector<double>::fromStdVector(profile.getValueVectorSampled());
    graph->setData(x, y);
    graph->rescaleAxes(isReScaled);
    if(!isReScaled)
        isReScaled = true;
}

void ProfilePlotter::addPoints(std::vector<Profile::Point> points)
{
    QVector<double> x, y;
    for(auto &p : points) {
        x << p.first;
        y << p.second;
    }
    QCPGraph* graph = customPlot->addGraph();
    graph->setPen(pen);
    graph->setLineStyle(QCPGraph::lsNone);
    graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCross));
    graph->setData(x, y);
}

void ProfilePlotter::clearAll()
{
    customPlot->clearGraphs();
    isReScaled = false;
}

void ProfilePlotter::replot()
{
    customPlot->replot();
}



